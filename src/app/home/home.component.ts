import { Component, OnInit } from '@angular/core';
import { Persona } from '../interfaces/persona';
import { PersonasService } from '../services/personas.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  personas: Persona[];
  constructor(private personasService: PersonasService) {
    this.personasService.get().subscribe((data: Persona[])=>{
      this.personas=data;
    },(error)=>{
      console.log(error);
      alert('A ocurrido un error');
    });
   }

  ngOnInit(): void {
  }
  delete(id){
    this.personasService.delete(id).subscribe((data)=>{
      alert('eliminado con exito!');
    },(error)=>{
      console.log(error);
    });
  }

}
