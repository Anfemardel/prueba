export interface Persona {
    id?: number;
    nombres: string;
    apellidos: string;
    direccion: string;
    telefono: number;
    email: string;
    created_at?: string;
    update_at?: string;
}