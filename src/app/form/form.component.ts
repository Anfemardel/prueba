import { Component, OnInit } from '@angular/core';
import { Persona } from '../interfaces/persona';
import { PersonasService } from '../services/personas.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
  persona: Persona ={
    nombres: null,
    apellidos: null,
    direccion: null,
    telefono: null,
    email: null
  };
  id: any;
  editing: boolean = false;
  personas: Persona[];
  constructor(private personasService: PersonasService, private activatedRoute: ActivatedRoute) {
    this.id = this.activatedRoute.snapshot.params['id'];
    if(this.id){
      this.editing = true;
      this.personasService.get().subscribe((data: Persona[])=>{
        this.personas=data;
        this.persona= this.personas.find((p)=>{
          return p.id == this.id
      });},(error)=>{
        console.log(error);
      });
    }else{
      this.editing = false;
    }
   }

  ngOnInit(): void {
  }
  savePersona(){
    if(this.editing){
      this.personasService.put(this.persona).subscribe((data)=>{
        alert('Usuario Actualizado');
      },(error)=>{
        console.log(error);
        alert('Ocurrio Error');
      });
    }else{
      this.personasService.save(this.persona).subscribe((data)=>{
        alert('Usuario Guardado');
      },(error)=>{
        console.log(error);
        alert('Ocurrio Error');
      });
    }
  }
}
